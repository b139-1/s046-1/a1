import {Fragment} from "react";
import './App.css';
import {Container} from "react-bootstrap";

/*components*/
import AppNavbar from "./components/AppNavbar";

/*pages*/
import Home from "./pages/Home";
import Courses from "./pages/Courses"

function App() {
  return (
    <Fragment>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
    </Fragment>
  )
}

export default App;
