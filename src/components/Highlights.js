import {Container, Row, Col, Card} from "react-bootstrap";

const Hightlights = () => {
	return (
		<Container fluid className="mb-4">
			<Row>
				{/*Card 1*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
						<Card.Body>
							<Card.Title>Learn from Home</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				{/*Card 2*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
						<Card.Body>
							<Card.Title>Study Now, Pay Later</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				{/*Card 3*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
						<Card.Body>
							<Card.Title>Be Part of Our Community</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.Lorem ipsum dolor sit amet consectetur adipisicing, elit. Maiores adipisci, ducimus totam eius. Error maiores hic, nihil autem eos deleniti reprehenderit, eum voluptas minima pariatur consequatur, quis aspernatur. Natus, inventore.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>			
		</Container>
	)	
}

export default Hightlights;