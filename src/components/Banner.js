function Banner(){
	return (
		<div className="container-fluid mt-4">
			<div className="row justify-content-center">
				<div className="col-10 col-md-8">
					<div className="jumbotron">
						<h1>Welcome to React-Booking App!</h1>
						<p>Opportunities for everyone, everywhere!</p>
						<button className="btn btn-primary">Enroll</button>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Banner;