import {useState} from "react";
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import PropTypes from "prop-types";

const CourseCard = ({courseProp}) => {
	console.log(courseProp)
	const {name, description, price} = courseProp
	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	/*SYNTAX
		const [getter, setter] = useState(initialGetterValue)
	*/

	const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)

	const enroll = () => {
		setCount(count + 1);
		setSeat(seat - 1)
		if(seat === 0 && count === 30) {
			alert("Sorry\nNo more seats available!");
			setSeat(seat)
			setCount(count)
		}
	}

	return (
		<Container className="mb-4">
			<Row>
				<Col>
					<Card>
					  <Card.Title className="px-3 pt-4">{name}</Card.Title>
					  <Card.Body>
					    	<Card.Subtitle>Description:</Card.Subtitle>
				    		<Card.Text>{description}</Card.Text>
				    		<Card.Subtitle>Price:</Card.Subtitle>
				    		<Card.Text>{price}</Card.Text>
				    		<Card.Subtitle>Enrollees: </Card.Subtitle>
				    		<Card.Text>{count}</Card.Text>
				    		<Card.Subtitle>Seats available: </Card.Subtitle>
				    		<Card.Text>{seat}</Card.Text>
					    	<Button variant="primary" onClick={enroll}>Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
export default CourseCard;

// Check if the COurseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next

CourseCard.propTypes = {
	// shape method => is used to check if a prop object conforms to a specific "shape"
	courseProp: PropTypes.shape({
		// define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}