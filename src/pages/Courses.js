//import {Container} from "react-bootstrap"
import courseData from "./../data/courseData"
import CourseCard from "./../components/CourseCard"

const Courses = () => {
	//console.log(courseData)

	const courses = courseData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course}/>
		)
	})

	return (
		<>
			{courses}
		</>
	)
}

export default Courses;