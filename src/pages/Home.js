import {Fragment} from "react";
import Banner from "./../components/Banner";
import Highlights from "./../components/Highlights";

const Home = () => {
	return (
		<Fragment>
		  <Banner/>
		  <Highlights/>
		</Fragment>
	)
}

export default Home;